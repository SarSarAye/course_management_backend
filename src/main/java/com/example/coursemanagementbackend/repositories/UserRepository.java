package com.example.coursemanagementbackend.repositories;

import com.example.coursemanagementbackend.entity.Roles;
import com.example.coursemanagementbackend.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users,Long> {

    Optional<Users> findByUsername(String name);
    List<Users> findByRole(Roles role );

}
