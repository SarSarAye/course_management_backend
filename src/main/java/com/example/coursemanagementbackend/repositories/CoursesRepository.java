package com.example.coursemanagementbackend.repositories;

import com.example.coursemanagementbackend.entity.Courses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CoursesRepository extends JpaRepository<Courses,Long> {

    Optional<Courses> findByCourseName(String name);
    Optional<Courses> findById(Long id);




    @Query(value = "SELECT a.username,b.course_name from course_link c JOIN users_a ON c.user_id = a.id"+
            "JOIN COURSES b ON c.courses_id = b.id",nativeQuery = true)
    List<Object[]> findStudentCourses(Long id);
}
