package com.example.coursemanagementbackend.controller;

import com.example.coursemanagementbackend.dto.ResourceNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ResourceExceptionController {
    @ExceptionHandler(value = ResourceNotFound.class)
    public ResponseEntity<Object> exception(ResourceNotFound exception) {
        return new ResponseEntity<>("Resource not found", HttpStatus.NOT_FOUND);
    }
}
