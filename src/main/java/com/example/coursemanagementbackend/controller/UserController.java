package com.example.coursemanagementbackend.controller;

import com.example.coursemanagementbackend.Service.UserService;
import com.example.coursemanagementbackend.dto.AuthResponse;
import com.example.coursemanagementbackend.dto.GenericResponseDto;
import com.example.coursemanagementbackend.dto.UserDto;
import com.example.coursemanagementbackend.entity.Courses;
import com.example.coursemanagementbackend.entity.Roles;
import com.example.coursemanagementbackend.entity.Users;
import com.example.coursemanagementbackend.repositories.CoursesRepository;
import com.example.coursemanagementbackend.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final String COURSEMGT_USER_v1 = "1.0/courseManagement/user";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @PostMapping(COURSEMGT_USER_v1+"/register")
    public ResponseEntity<Void> createUser(@Valid @RequestBody UserDto userDto) throws  Exception{
        GenericResponseDto response = new GenericResponseDto();
        Optional<Users> user = userRepository.findByUsername(userDto.getUsername());
        
        if(user.isPresent()){
            LOGGER.debug("User with username {} alrady exists",userDto.getUsername());
            response.setMessage(("User already exists"));
            return new ResponseEntity(response,HttpStatus.FORBIDDEN);
        }
        userService.createUser(userDto.getUsername(),userDto.getEmail(),userDto.getRole(),userDto.getPassword());
        response.setMessage("User Created.");
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PostMapping(COURSEMGT_USER_v1+"/authenticate")
    public ResponseEntity<AuthResponse> login(@Valid @RequestBody SingInInfo singInInfo) throws Exception {
        LOGGER.info("User with email {} signing in", singInInfo.getUsername());
        AuthResponse authResponse = userService.login(singInInfo.getUsername(), singInInfo.getPassword());
        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }

    //get course
    @GetMapping(COURSEMGT_USER_v1+"/allStudents")
    public ResponseEntity<Void>  getAllStudents(){
        GenericResponseDto response = new GenericResponseDto();

//      List<Users> user = userRepository.findAll();
//      user.stream().filter(p->p.getRole().equals(Roles.STUDENT));
        List<Users> user = userRepository.findByRole(Roles.STUDENT);

        if(user.isEmpty()){
            response.setMessage(("No Students"));
            return new ResponseEntity(response,HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity(user,HttpStatus.OK);
    }


    @PutMapping(COURSEMGT_USER_v1+"/assignStudent")
    public ResponseEntity<Void> assign(@RequestBody AssignDto assignDto) throws  Exception {
        GenericResponseDto response = new GenericResponseDto();
        userService.assignStudent(assignDto.userId,assignDto.studentList);
        response.setMessage("Successfully assigned.");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

static class SingInInfo {
    @NotBlank
    String username;
    @NotBlank
    String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

    static class AssignDto {
        @NotNull
        private Long userId;
        @NotNull
        private List<Long> studentList;

        public AssignDto(@NotBlank Long userId, @NotBlank List<Long> studentList) {
            this.userId = userId;
            this.studentList = studentList;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public List<Long> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<Long> studentList) {
            this.studentList = studentList;
        }
    }

}


