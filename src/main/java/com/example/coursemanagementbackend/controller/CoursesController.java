package com.example.coursemanagementbackend.controller;

import com.example.coursemanagementbackend.Service.CoursesService;
import com.example.coursemanagementbackend.dto.*;
import com.example.coursemanagementbackend.entity.Courses;
import com.example.coursemanagementbackend.entity.Users;
import com.example.coursemanagementbackend.repositories.CoursesRepository;
import com.example.coursemanagementbackend.repositories.UserRepository;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class CoursesController {

    @Autowired
    private CoursesRepository coursesRepository;

    @Autowired
    private CoursesService coursesService;

    @Autowired
    private UserRepository userRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final String COURSEMGT_COURSE_v1 = "1.0/courseManagement/course";

    //get course
    @GetMapping(COURSEMGT_COURSE_v1+"/allCourses")
    public List<Courses> getAllCourses(){
        return this.coursesRepository.findAll();
    }

    //get course by id
    @GetMapping(COURSEMGT_COURSE_v1+"/courses/{courseId}")
    public ResponseEntity<Courses> getCoursesById(@PathVariable("courseId") Long courseId) throws Exception{
            Courses courses= coursesRepository.findById(courseId).orElseThrow(EntityNotFoundException::new);
            return new ResponseEntity<>(courses,HttpStatus.OK);
    }

//    //get course by student name
    @GetMapping(COURSEMGT_COURSE_v1+"/student/{studentName}")
    public ResponseEntity<List<StudentCoursesDto>> getCourseByStudentName(@PathVariable("studentName") String studentName) throws Exception{

        Users student =  userRepository.findByUsername(studentName).orElseThrow(EntityNotFoundException::new);
        List<StudentCoursesDto> userResponses = new ArrayList<>();
        if (Objects.nonNull(student)) {
            userResponses = student.getLinkedCourses().stream()
                    .map(u -> new StudentCoursesDto(u.getCourseName(),u.getAssignedUsersStudent().toString()))
                    .collect(Collectors.toList());
        }

        return new ResponseEntity(userResponses,HttpStatus.OK);
    }

    // update course by id
    @PatchMapping(COURSEMGT_COURSE_v1+"/update/{courseId}")
    public ResponseEntity<CoursesDto> updateCourse(@RequestBody CoursesDto coursesDto ) throws ResourceNotFound {
            GenericResponseDto response = new GenericResponseDto();

            Courses course = coursesRepository.findById(coursesDto.getId())
                    .orElseThrow(() ->new ResourceNotFound());

            course.setCourseName(coursesDto.getCoursesname());
            coursesRepository.save(course);
            response.setMessage("Updated course");
            return new ResponseEntity(response,HttpStatus.OK);
    }

    //create course
    @PostMapping(COURSEMGT_COURSE_v1+"/create")
    public ResponseEntity<Void> createCourse(@Valid @RequestBody NewCourse coursesDto) throws  Exception{
        GenericResponseDto response = new GenericResponseDto();
        Optional<Courses> course = coursesRepository.findByCourseName(coursesDto.getCoursesname());

        if(course.isPresent()){
            LOGGER.debug("course with courseName {} already exists",coursesDto.getCoursesname());
            response.setMessage(("Course already exists"));
            return new ResponseEntity(response, HttpStatus.FORBIDDEN);
        }
        coursesService.createCourse(coursesDto.getCoursesname());
        response.setMessage("Course added.");
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @DeleteMapping(COURSEMGT_COURSE_v1+"/delete/{id}")
    public ResponseEntity<Void> deleteCourse(@PathVariable("id") Long id)throws  Exception{
        GenericResponseDto response = new GenericResponseDto();
        Optional<Courses> course = Optional.ofNullable(coursesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFound()));

        Collection<String> studentList = course.get().getAssignedUsersStudent();

        course.ifPresent( c ->{
            if(studentList != null && studentList.size()>0){
                studentList.forEach(username ->{
                    Users s = userRepository.findByUsername(username).get();
                    c.getAssignedUsersStudent().remove(s.getId());
                    s.getLinkedCourses().remove(c);
                });
            }
            System.out.println(c);
        this.coursesRepository.delete(c);
        });
        response.setMessage("Deleted");
       return new ResponseEntity(response,HttpStatus.OK);
    }

    static class NewCourse {

        @NotBlank
        String coursesname;

        public String getCoursesname() {
            return coursesname;
        }

        public void setCoursesname(String coursesname) {
            this.coursesname = coursesname;
        }
    }

    static class StudentCoursesDto{

        @JsonProperty("course")
        private String course;

        @JsonProperty("username")
        private String username;

        public StudentCoursesDto() {

        }

        public String getCourse() {
            return course;
        }

        public void setCourse(String course) {
            this.course = course;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public StudentCoursesDto(String course, String username) {
            this.course = course;
            this.username = username;
        }
    }

}
