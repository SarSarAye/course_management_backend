package com.example.coursemanagementbackend.Service;

import com.example.coursemanagementbackend.entity.Courses;
import com.example.coursemanagementbackend.repositories.CoursesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoursesService {

    @Autowired
    private CoursesRepository coursesRepository;

    public  void createCourse(String courses)throws  Exception{
       coursesRepository.save(new Courses(courses));
    }

//    public List<StudentCoursesDto> findCourses(Long id)throws Exception{
//       List<Object[]> coursesList= coursesRepository.findStudentCourses(id);
//        List<StudentCoursesDto> listCourses = new ArrayList<>();
//        for(Object[] obj: coursesList){
//            System.out.println(obj[0].toString());
//            StudentCoursesDto studentCoursesDto = new StudentCoursesDto();
//            //obj[0] value is the first value get from query in repo
//            studentCoursesDto.setCourse(obj[0].toString());
//            //obj[0] value is the 2nd value get from query in repo
//            studentCoursesDto.setUsername(obj[1].toString());
//            listCourses.add(studentCoursesDto);
//        }
//
//        return listCourses;
//
//    }


}
