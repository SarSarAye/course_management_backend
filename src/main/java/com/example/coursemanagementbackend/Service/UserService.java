package com.example.coursemanagementbackend.Service;

import com.example.coursemanagementbackend.dto.AuthResponse;
import com.example.coursemanagementbackend.entity.Courses;
import com.example.coursemanagementbackend.entity.Roles;
import com.example.coursemanagementbackend.entity.Users;
import com.example.coursemanagementbackend.repositories.CoursesRepository;
import com.example.coursemanagementbackend.repositories.UserRepository;
import com.example.coursemanagementbackend.security.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CoursesRepository coursesRepository;

    @Autowired
    private JwtUtil jwtUtil;

    public  void createUser(String username,
                            String email,
                            String role,
                            String password) throws  Exception{

        userRepository.save(new Users(username,email, Roles.valueOf(role),passwordEncoder.encode(password)));

    }

    public void assignStudent(Long studentId, List<Long> courseIdList){
        Optional<Users> user = userRepository.findById(studentId);
        user.ifPresent( s ->{
            if(courseIdList != null && courseIdList.size()>0){
                courseIdList.forEach(id ->{
                    Courses c = coursesRepository.findById(id).get();
                            c.getAssignedUsersStudent().add(s.getUsername());
                            s.getLinkedCourses().add(c);
                });
            }
            userRepository.save(s);
        });
    }

    public AuthResponse login(String username, String password) throws Exception {
        Optional<Users> user = userRepository.findByUsername(username);
        if (!(user.isPresent()))
            throw new Exception("User does not exist");
        Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.get().getUsername(), password));
        String token = jwtUtil.createToken(user.get().getUsername(), user.get().getRole());
        Long expiration = jwtUtil.getExpirationFromToken(token).getTime();

        return new AuthResponse(token, user.get().getRole().roleName(), expiration);
    }

}
