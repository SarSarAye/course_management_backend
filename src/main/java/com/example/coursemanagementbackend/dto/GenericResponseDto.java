package com.example.coursemanagementbackend.dto;

public class GenericResponseDto {

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "GenericResponseDto{" +
                "message='" + message + '\'' +
                '}';
    }

}
