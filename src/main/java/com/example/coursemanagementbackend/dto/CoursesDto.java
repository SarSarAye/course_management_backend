package com.example.coursemanagementbackend.dto;

import javax.validation.constraints.NotBlank;

public class CoursesDto {

    @NotBlank
    private Long id;

    @NotBlank
    private String coursesname;

    public CoursesDto(Long id, String coursesname) {
        this.id = id;
        this.coursesname = coursesname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoursesname() {
        return coursesname;
    }

    public void setCoursesname(String coursesname) {
        this.coursesname = coursesname;
    }
}
