package com.example.coursemanagementbackend.dto;

import java.io.Serializable;

public class AuthResponse implements Serializable {

    private final String token;
    private long expiration;
    private String role;

    public AuthResponse(String token, String role, long expiration) {
        this.token = token;
        this.role = role;
        this.expiration = expiration;
    }

    public String getToken() {
        return token;
    }

    public  String getRole() {
        return role;
    }

    public long getExpiration() {
        return expiration;
    }
}

