package com.example.coursemanagementbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Entity
public class Courses {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;

    @Column(name = "courseName")
    private String courseName;

    @JsonIgnore
    @ManyToMany(mappedBy = "linkedCourses")
    List<Users> links = new ArrayList<>();

//    @OneToMany(mappedBy = "courses")
//    Set<CourseRegistration> assign;

    @ElementCollection
    @CollectionTable(name = "student_list", joinColumns = @JoinColumn(name = "student_list_id"))
    @Column(name = "student_username")
    private Collection<String> assignedUsersStudent  = new ArrayList<>();

    @CreationTimestamp
    private LocalDateTime created;

    @UpdateTimestamp
    private LocalDateTime modified;

    public Courses(){}

    public Courses(String courseName){
        this.courseName= courseName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

//    public Set<CourseRegistration> getAssign() {
//        return assign;
//    }
//
//    public void setAssign(Set<CourseRegistration> assign) {
//        this.assign = assign;
//    }

    public void addLinks(Users user){
        links.add(user);
        user.getLinkedCourses().add(this);
    }

    public void removeLinks(Users user){
        links.remove(user);
        user.getLinkedCourses().remove(this);
    }

    public Collection<String> getAssignedUsersStudent() {
        return assignedUsersStudent;
    }

    public void setAssignedUsersStudent(Collection<String> assignedUsersStudent) {
        this.assignedUsersStudent = assignedUsersStudent;
    }

    public List<Users> getLinks() {
        return links;
    }

    public void setLinks(List<Users> links) {
        this.links = links;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Courses courses = (Courses) o;

        return new EqualsBuilder()
                .append(id, courses.id)
                .append(courseName, courses.courseName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(courseName)
                .toHashCode();
    }



}
