package com.example.coursemanagementbackend.entity;

public enum Roles {

    TEACHER( "TEACHER"),
    STUDENT( "STUDENT");

    private String name;
    Roles(String name){ this.name = name; }
    public String roleName(){ return name; }


}
